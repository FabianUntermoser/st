#!/bin/sh

find_prompt(){
    grep "\S" "$1" | # grep any non-whitespace
    tail -n 1      | # get last line
    sed 's/^\s*//'   # remove whitespace
}

select_prompt(){
    prompt="$1"
    file="$2"
    grep -F "$prompt" "$file" | # find prompts
        tac | # reverse
        rofi -dmenu -p "Copy which command's output?" -i # select prompt
}

count_prompts(){
    prompt="$1"
    file="$2"
    grep -c -F "$ps1" "$tmpfile"
}

encode() { echo "$@" | sed 's/[^^]/[&]/g; s/\^/\\^/g'; }

tmpfile=$(mktemp /tmp/st-cmd-output.XXXXXX)
tmpfile2=$(mktemp /tmp/st-editcommand-cmd.XXXXXX)
trap 'rm -f "$tmpfile" "$tmpfile2"' 0 1 15

sed -n "w $tmpfile" # write pipe to file
sed -i 's/\x0//g' "$tmpfile" # remove nulls

ps1="$(find_prompt "$tmpfile")"
sed -i '$d' "$tmpfile" # remove empty prompt at end of file

[ $(count_prompts "$ps1" "$tmpfile") = 0 ] && exit

chosen="$(select_prompt "$ps1" "$tmpfile")"

[ -z "$chosen" ] && exit

chosen="$(encode "$chosen")"
ps1="$(encode "$ps1")"

awk "/^$chosen$/{p=1;print;next} p&&/$ps1/{p=0};p" "$tmpfile" >> "$tmpfile2"

"$TERMINAL" -e "$EDITOR" "$tmpfile2"
