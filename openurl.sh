#!/bin/sh

[ -z "$BROWSER" ] && exit

urlregex='(((http|https)://|www\\.)[a-zA-Z0-9.]*[:]?[a-zA-Z0-9./@$&%?$#=_-]*)|((magnet:\\?xt=urn:btih:)[a-zA-Z0-9]*)'

# default to stdin if not input file was provided
file=${1:-/dev/stdin}

url=$(
    grep -aEo "$urlregex" "$file" | # find urls
    uniq | # remove duplicates
    tac # reverse
)

# keep this as an extra step to ensure system
# is not hanging when $file is not reachable
url=$(echo "$url" | rofi -dmenu -p "Open URL: " -i)

echo "$url"

[ -n "$url" ] &&
    setsid -f xdg-open "$url"
